package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements ShoppingCartOperation {

    private final List<Product> basket;

    public ShoppingCart() {
        this.basket = new ArrayList<Product>();
    }

    public boolean addProducts(String productName, int price, int quantity) {
        if(price <= 0 || quantity <= 0 || (getProductsQuantity()+quantity > PRODUCTS_LIMIT)) {
            return false;
        }

        for(Product product : basket) {
            if(product.getProductName().equals(productName)) {
                if(product.getPrice() == price) {
                    product.setQuantity(product.getQuantity() + quantity);
                    return true;
                } else {
                    return false;
                }
            }
        }
        basket.add(new Product(productName, price, quantity));
        return true;

    }

    public boolean deleteProducts(String productName, int quantity) {
        if(quantity <= 0) {
            return false;
        }

        for(Product product : basket) {
            if(product.getProductName().equals(productName)) {
                if(product.getQuantity() > quantity) {
                    product.setQuantity(product.getQuantity() - quantity);
                    return true;
                } else if (product.getQuantity() == quantity) {
                    basket.remove(product);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public int getQuantityOfProduct(final String productName) {
        for(Product product : basket) {
            if(product.getProductName().equals(productName)) {
                return product.getQuantity();
            }
        }
        return 0;
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for(Product product : basket) {
            sum += product.getPrice()*product.getQuantity();
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        for(Product product : basket) {
            if(product.getProductName().equals(productName)) {
                return product.getPrice();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        List<String> productsNames = new ArrayList<String>();
        for(Product product : basket) {
            productsNames.add(product.getProductName());
        }
        return productsNames;
    }

    private int getProductsQuantity() {
        int sum = 0;
        for(Product product : basket) {
            sum += product.getQuantity();
        }
        return sum;
    }

}