package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {

    @ParameterizedTest
    @CsvSource({
            "Apple, 12, 2",
            "Potato, 4, 20",
            "Peach, 18, 46",
            "Apple, 12, 4"
    })
    void checkCorrectAddProduct(String productName, int price, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        final boolean result = shoppingCart.addProducts(productName, price, quantity);
        assertTrue(result);
    }


    @ParameterizedTest
    @CsvSource({
            "Apple, 10, 0",
            "Potato, 0, 5",
            "Orange, 15, -1"
    })
    void checkIncorrectAddProduct(String productName, int price, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        final boolean result = shoppingCart.addProducts(productName, price, quantity);
        assertFalse(result);
    }

    @ParameterizedTest
    @CsvSource({
            "Apple, 4",
            "Potato, 2",
            "Orange, 10"
    })
    void checkCorrectDeleteProduct(String productName, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 10, 6);
        shoppingCart.addProducts("Potato", 10, 5);
        shoppingCart.addProducts("Orange", 10, 10);

        final boolean result = shoppingCart.deleteProducts(productName, quantity);
        assertTrue(result);
    }

    @ParameterizedTest
    @CsvSource({
            "Apple, 4",
            "Potato, -1",
            "Orange, 0",
            "Peach, 2"
    })
    void checkIncorrectDeleteProduct(String productName, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 10, 3);
        shoppingCart.addProducts("Potato", 10, 5);
        shoppingCart.addProducts("Orange", 10, 10);

        final boolean result = shoppingCart.deleteProducts(productName, quantity);
        assertFalse(result);
    }


    @ParameterizedTest
    @CsvSource({
            "Apple, 2",
            "Potato, 4",
            "Peach, 18",
            "Orange, 0"
    })
    void getQuantityOfProduct(String productName, int quantity) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 10, 2);
        shoppingCart.addProducts("Potato", 10, 4);
        shoppingCart.addProducts("Peach", 10, 18);
        final int result = shoppingCart.getQuantityOfProduct(productName);
        assertEquals(quantity, result);
    }

    @Test
    void getSumOfProductsPrices() {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 10, 3);
        shoppingCart.addProducts("Potato", 20, 5);
        shoppingCart.addProducts("Orange", 5, 4);
        final int result = shoppingCart.getSumProductsPrices();
        assertEquals(150, result);
    }

    @ParameterizedTest
    @CsvSource({
            "Apple, 10",
            "Potato, 20",
            "Orange, 5"
    })
    void getProductPrice(String productName, int price) {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 10, 3);
        shoppingCart.addProducts("Potato", 20, 5);
        shoppingCart.addProducts("Orange", 5, 4);
        final int result = shoppingCart.getProductPrice(productName);
        assertEquals(price, result);
    }

    @Test
    void getProductNames() {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Apple", 10, 3);
        shoppingCart.addProducts("Potato", 20, 5);
        shoppingCart.addProducts("Orange", 5, 4);

        List<String> names = Arrays.asList("Apple", "Potato", "Orange");

        final List<String> result = shoppingCart.getProductsNames();
        assertEquals(names, result);
    }

}
